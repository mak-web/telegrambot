<?php

function bot($method, $params = []){
    $url = 'https://api.telegram.org/bot' . TOKEN . '/' . $method;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);

    $res = curl_exec($ch);
    if(curl_error($ch)){
        curl_close($ch);
        var_dump(curl_error($ch));
    }
    else{
        curl_close($ch);
        return json_decode($res);
    }
}

?>