<?php

require_once "CONSTS.php";
require_once "DB.php";
require_once "bot.php";
require_once "iq300.php";
require_once "authorize.php";
require_once "createTask.php";


$output = json_decode(file_get_contents('php://input'), TRUE);
$message = $output['message'];
$chat_id = $message['chat']['id'];
$user_id = $message['from']['id'];
$text = isset($message['text'])? $message['text'] : $message['forward_from']['text'];

if($text == "/help"){
    bot('sendMessage', ['chat_id' => $chat_id, 'text' => HELPINSTRUCTION]);
}
else{

    $authorizeBool = authorizeBool($user_id);

    if(!$authorizeBool){
        authorize($user_id, $chat_id, $text);
    }
    else{
        createTask($chat_id, $user_id, $text, $authorizeBool);
    }
}

?>
