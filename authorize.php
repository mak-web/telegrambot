<?php

function authorizeBool($user_id){

    $iq300Tokens = getIq300tokens();
    foreach ($iq300Tokens as $iq300Token){
        if($user_id == $iq300Token['user_id']) {
            return $iq300Token;
        }
    }
    return false;
}

function authorize($user_id, $chat_id, $text){
    $params = authorizeLogic($user_id, $chat_id, $text);
    bot('sendMessage', $params);
}

function authorizeLogic($user_id, $chat_id, $text){
    $firstAuthorizeUsers = getFirstAuthorizeUsers();
    $returnParams = [];
    if($firstAuthorizeUsers->num_rows > 0){
        foreach($firstAuthorizeUsers as $firstAuthorizeUser) {
            if($user_id == $firstAuthorizeUser['user_id']){
                $userData = explode(':', $text);
                $params = http_build_query(['email' => $userData[0], 'password' => $userData[1]]);
                $res = iq300c('api/v2/sessions/', $params, true, true);
                switch ($res['info']){
                    case 422:
                        $returnParams = ['chat_id' => $chat_id, 'text' => INCORRECTEMAILORPASSWORD];
                        //deleteFirstAuthorizeUser($user_id);
                        break;
                    case 500:
                        $returnParams = ['chat_id' => $chat_id, 'text' => ERROR];
                        //deleteFirstAuthorizeUser($user_id);
                        break;
                    case 200:
                        $access_token = $res['result']->access_token;
                        deleteFirstAuthorizeUser($user_id);
                        setIq300tokens($user_id, $access_token);
                        setCreateTasksUserId($user_id, 0);

                        $access_token = http_build_query(['access_token'=>$access_token]);
                        $url = 'https://app.iq300.ru/api/v2/users/current?' . $access_token;
                        // костыль
                        $current_user = iq300f($url);
                        $short_name = $current_user->user->short_name;
                        $returnParams = ['chat_id' => $chat_id, 'text' => (SUCCESFULLYAUTHORIZE . $short_name . NOTIFACATION . SHORTINSTRUCTION)];
                        break;
                }
                return $returnParams;
            }
        }

        $returnParams = ['chat_id' => $chat_id, 'text' => GETUSERDATAFORAUTHORIZE];
        setFirstAuthorizeUserUserId($user_id);
        return $returnParams;
    }
}



/*function authorizeLogic($user_id, $chat_id, $text){
    $firstAuthorizeUsers = getFirstAuthorizeUsers();
    if($firstAuthorizeUsers->num_rows > 0){
        foreach($firstAuthorizeUsers as $firstAuthorizeUser) {
            if (($user_id == $firstAuthorizeUser['user_id']) && ($firstAuthorizeUser['login'] != NULL)) {
                $login = $firstAuthorizeUser['login'];
                $password = $text;
                $params = http_build_query(['email' => $login, 'password' => $password]);
                $res = iq300c('api/v2/sessions/', $params, true, true);
                $returnParams = '';
                switch ($res['info']){
                    case 422:
                        $returnParams = ['chat_id' => $chat_id, 'text' => INCORRECTEMAILORPASSWORD];
                        deleteFirstAuthorizeUser($user_id);
                        break;
                    case 500:
                        $returnParams = ['chat_id' => $chat_id, 'text' => ERROR];
                        deleteFirstAuthorizeUser($user_id);
                        break;
                    case 200:
                        $access_token = $res['result']->access_token;
                        deleteFirstAuthorizeUser($user_id);
                        setIq300tokens($user_id, $access_token);

                        $access_token = http_build_query(['access_token'=>$access_token]);
                        $url = 'https://app.iq300.ru/api/v2/users/current?' . $access_token;
                        // костыль
                        $current_user = iq300f($url);
                        $short_name = $current_user->user->short_name;
                        $returnParams = ['chat_id' => $chat_id, 'text' => (SUCCESFULLYAUTHORIZE . $short_name)];

                        break;
                }
                var_dump($res);
                return $returnParams;
            }
            else if($user_id == $firstAuthorizeUser['user_id']){
                setFirstAuthorizeUserLogin($user_id, $text);
                $returnParams = ['chat_id' => $chat_id, 'text' => GETPASSWORDFROMUSER];
                return $returnParams;
            }
        }
    }
    else{
        $returnParams = ['chat_id' => $chat_id, 'text' => 'Internal error 10'];
        return $returnParams;
    }

    $returnParams = ['chat_id' => $chat_id, 'text' => GETLOGINFROMUSER];
    setFirstAuthorizeUserUserId($user_id);
    return $returnParams;
}*/


