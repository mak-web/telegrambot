<?php

function iq300c($method, $params =[], $post = false, $info = false)
{
    $url = "https://app.iq300.ru/" . $method;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    if ($post) {
        curl_setopt($ch, CURLOPT_POST, true);
    }

    $res = curl_exec($ch);
    echo "<pre>";
    if (curl_error($ch)) {
        var_dump(curl_error($ch));
        curl_close($ch);
    }
    else {
        if($info){
            $returnInfo = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
            return ['result' => json_decode($res), 'info' => $returnInfo];
        }
        else{
            curl_close($ch);
            return json_decode($res);
        }
    }
}

function iq300f($url){
    return json_decode(file_get_contents($url));
}