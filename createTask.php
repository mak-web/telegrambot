<?php

function createTask($chat_id, $user_id, $text, $authorizeBool){
    $params = createTaskLogic($chat_id, $user_id, $text, $authorizeBool);
    bot('sendMessage', $params);
}

function createTaskLogic($chat_id, $user_id, $text, $authorizeBool){
    $iq300Token = $authorizeBool;
    $access_token = $iq300Token['access_token'];
    $access_token = http_build_query(['access_token'=>$access_token]);
    $returnParams = [];

    $createTasks = getCreateTasks();
    foreach($createTasks as $createTask){
        if($user_id == $createTask['user_id']){
            switch ($createTask['stage']) {
                // Отправляет запрос на Project_id
                case 0:
                    preg_match('/([@])(.*)/', $text, $forwardHeader);

                    if(count($forwardHeader[1]) == 0 ){
                        myHeaderAndDescription($user_id, $text);
                        $returnParams = getProjectFromUser($chat_id, $access_token);
                    }
                    else{
                        myHeader($user_id, $forwardHeader[2], 1);
                    }
                    break;
                case 1:
                    description($user_id, $text, 2);
                    $returnParams = getProjectFromUser($chat_id, $access_token);
                    break;
                // Отправляет запрос на Executor_id
                case 2:
                    if ((int)$text != 0) {
                        $url = 'https://app.iq300.ru/api/v2/projects?' . $access_token;
                        $projects = iq300f($url)->projects;
                        $project_id = $projects[(int)$text - 1]->id;
                        project_id($user_id, $project_id, 3);

                        $url = 'https://app.iq300.ru/api/v2/projects/' . $project_id . '/members?' . $access_token;
                        $executors = iq300f($url)->members;
                        $i = 0;
                        $executorsText = '';
                        foreach ($executors as $executor) {

                            $executorsText .= $i . ' - ' . $executor->user->short_name . ' | ';
                            $i++;
                        }
                        $returnParams = ['chat_id' => $chat_id, 'text' => (GETEXECUTORIDSFROMUSER . $executorsText)];
                    }
                    else {
                        project_id($user_id, $text, 4);
                        $url = 'https://app.iq300.ru/api/v2/communities/14890/executors?' . $access_token;
                        $executorsArray = iq300f($url);
                        $i = 0;
                        $executorsText = '';
                        foreach ($executorsArray as $executors) {
                            foreach ($executors as $executor) {
                                foreach ($executor->users as $user) {
                                    $executorsText .= $i . ' - ' . $user->name . ' | ';
                                    $i++;
                                }
                            }
                        }
                        $returnParams = ['chat_id' => $chat_id, 'text' => (GETEXECUTORIDSFROMUSER . $executorsText)];
                    }

                    break;
                case 3:
                    $result = createWithProject($user_id, $access_token, $text);
                    switch ($result['info']) {
                        case 200:
                            $title = 'Заголовок: ' . $result['result']->task->title . "\r\n";
                            $description = 'Описание: ' . $result['result']->task->description . "\r\n";
                            $project = 'Проект: ' . $result['result']->task->project->title . "\r\n";
                            $executor = 'Исполнитель: ' . $result['result']->task->executor->short_name . "\r\n";
                            $refactorUrl = 'Ссылка на задачу: https://app.iq300.ru/tasks/' . $result['result']->task->id;
                            $returnParams = ['chat_id' => $chat_id, 'text' => (SUCCESSCREATEWITHPROJECT . $title . $description . $project .  $executor . $refactorUrl)];
                            break;
                        default:
                            $returnParams = ['chat_id' => $chat_id, 'text' => $result['info']];
                            break;
                            break;
                    }
                    deleteCreateTask($user_id);
                    break;
                case 4:
                    $url = 'https://app.iq300.ru/api/v2/communities/14890/executors?' . $access_token;
                    $executorsArray = iq300f($url);

                    $userExecutorId = (int)$text;
                    $executor_id = $executorsArray->executors[0]->users[0]->id;

                    foreach ($executorsArray as $executors){
                        foreach ($executors as $executor){
                            foreach ($executor->users as $user){
                                if($userExecutorId < count($executor->users)){
                                    $executor_id = $executor->users[$userExecutorId]->id;
                                }
                                else{
                                    $userExecutorId = $userExecutorId - count($executor->users);
                                }
                            }
                        }
                    }

                    $result = createWithoutProject($user_id, $access_token, $executor_id);
                    switch ($result['info']) {
                        case 200:
                            $title = 'Заголовок: ' . $result['result']->tasks[0]->title . "\r\n";
                            $description = 'Описание: ' . $result['result']->tasks[0]->description . "\r\n";
                            $project = 'Проект не выбран' . "\r\n";
                            $executor = 'Исполнитель: ' . $result['result']->tasks[0]->executor->short_name . "\r\n";
                            $refactorUrl = 'Ссылка на задачу: https://app.iq300.ru/tasks/' . $result['result']->tasks[0]->id;
                            $returnParams = ['chat_id' => $chat_id, 'text' => (SUCCESSCREATEWITHOUTPROJECT . $title . $description . $project . $executor . $refactorUrl)];
                            break;
                        default:
                            $returnParams = ['chat_id' => $chat_id, 'text' => $result['info']];
                            break;
                    }
                    deleteCreateTask($user_id);
                    break;
                default:
                    deleteCreateTask($user_id);
                    break;
            }
            return $returnParams;
        }
    }

    $url = 'https://app.iq300.ru/api/v2/users/current?' . $access_token;
    $current_user = iq300f($url);
    $short_name = $current_user->user->short_name;
    $executor_id = $current_user->user->id;
    $returnParams = ['chat_id' => $chat_id, 'text' => (ENTERAS . $short_name . ". " . SHORTINSTRUCTION)];
    setCreateTasksUserId($user_id, 0);
    return $returnParams;
}

function myHeaderAndDescription($user_id, $text){
    if((preg_match("[<]", $text)) && (preg_match("[>]", $text))){
        preg_match("/[<](.*?)[>]/", $text, $header);
        myheader($user_id, $header[1], 2);
        $description = ltrim(substr(stristr($text, '>'), 1 ));
        description($user_id, $description, 2);
    }
    else{
        $header = substr($text, 0, 30);
        myheader($user_id, $header, 2);
        $description = substr($text, 30);
        description($user_id, $description, 2);
    }
}

function myheader($user_id, $header, $stage){
    setCreateTasksHeader($user_id, $header, $stage);
}
function description($user_id, $description, $stage){
    setCreateTasksDescription($user_id, $description, $stage);
}

function project_id($user_id, $text, $stage){
    setCreateTasksProjectId($user_id, (int)$text, $stage);
}

function getProjectFromUser($chat_id, $access_token){
    $url = 'https://app.iq300.ru/api/v2/projects?' . $access_token;
    $projects = iq300f($url)->projects;
    $titles = '0 - без проекта';
    $i = 1;
    foreach ($projects as $project) {
        $titles .= ', ' . $i . ' - ' . $project->title;
        $i++;
    }
    $returnParams = ['chat_id' => $chat_id, 'text' => GETPROJECTIDFROMUSER . $titles];
    return $returnParams;
}

function createWithoutProject($user_id, $access_token, $executor){
    $createtask = mysqli_fetch_array(getCreateTaskByUserId($user_id));
    $community_id = iq300f('https://app.iq300.ru/api/v2/tasks/communities?' . $access_token)->communities[0]->id;
    $params = http_build_query((['task' => [
        'title' => $createtask['header'],
        'description' => $createtask['description'],
        'community_id' => $community_id,
        'end_date' => '2017-12-18T13:32:47+00:00',
        'executor_id' => $executor
    ]]));
    return $res = iq300c('api/v2/tasks?' . $access_token, $params, true, true);
}

function createWithProject($user_id, $access_token, $text){
    $createtask = mysqli_fetch_array(getCreateTaskByUserId($user_id));
    $project_id = $createtask['project_id'];
    $url = 'https://app.iq300.ru/api/v2/projects/' . $project_id . '/members?' . $access_token;
    $executors = iq300f($url)->members;
    $executor_id = $executors[(int)$text]->user->id;
    $params = http_build_query((['task' => [
        'title' => $createtask['header'],
        'description' => $createtask['description'],
        //'end_date' => '2017-12-18T13:32:47+00:00',
        'executor_id' => $executor_id
    ]]));
    return $res = iq300c('api/v2/projects/'. $project_id . '/tasks?' . $access_token, $params, true, true);
}


